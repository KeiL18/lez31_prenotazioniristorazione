Creare un sistema di gestione prenotazioni al ristorante:

Una prenotazione è caratterizzata da:
- Numero di prenotazione (random gestito in automatico)
- Nome
- Cognome
- Data e orario

Creare:
- Database SQL
- Spring Boot App in RESTFul
- Angular APP per la visualizzazione e l'inserimento dei dati
Nella lista in FrontEnd ordinare le prenotazioni in base a quella più vicina all'orario attuale.