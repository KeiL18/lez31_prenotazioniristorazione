package com.lezione31.PrenotazioniRistorante.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.sql.Date;

public class Prenotazione {
	
	private Integer id;
	private String nome;
	private String cognome;
	private LocalDateTime orario;
	private String codice;
	
	public Prenotazione() {
		
	}
	public Prenotazione(String varNome, String varCognome, String varOrario) {
		this.setNome(varNome);
		this.setCognome(varCognome);
		this.setOrario(varOrario);
		this.setCodice();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public Date getOrario() {
		Date orario = Date.valueOf(this.orario.toLocalDate());
		return orario;
	}
	public String getOrarioS() {
		return "" + this.orario;
	}
	public void setOrario(String orario) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
		LocalDateTime dateTime = LocalDateTime.parse(orario, formatter);
		this.orario = dateTime; 
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice.toUpperCase();
	}
	public void setCodice() {
		this.codice = generaCodice();
	}
	
	/**
	 * Metodo per la generazione di un codice di prenotazione più univoco possibile.
	 * Prende le prime 3 lettere del cognome, la data e l'ora nel formato "XXXYYYYMMGGhhmmss"
	 * @param tempData data della prenotazione in CALENDAR format
	 * @return Una stringa contenente il codice univoco
	 */
	private String generaCodice() {
		return  this.getCognome().toUpperCase().substring(0,2) + this.orario.getYear() + this.orario.getMonthValue() + this.orario.getDayOfMonth() + this.orario.getHour() + this.orario.getMinute()+ this.orario.getSecond();
	}
}
