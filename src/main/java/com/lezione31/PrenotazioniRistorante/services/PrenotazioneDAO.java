package com.lezione31.PrenotazioniRistorante.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione31.PrenotazioniRistorante.connessione.ConnettoreDB;
import com.lezione31.PrenotazioniRistorante.models.Prenotazione;

public class PrenotazioneDAO implements Dao<Prenotazione>{
	
	/**
	 * Metodo per l'estrazione dal DB della prenotazione, tramite una ricerca per ID.
	 * @param ID di ricerca
	 * @return Ritorna un oggetto Prenotazione contentente tutte le informazioni.
	 */
	@Override
	public Prenotazione getById(int ID) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		Prenotazione temp = new Prenotazione();
		
		String query = "SELECT id, nome, cognome, orario, numeroPrenot FROM Prenotazione WHERE id = ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setInt(1, ID);
		
		ps.executeQuery();
		
		ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	temp.setId(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setCognome(risultato.getString(3));
		temp.setOrario(risultato.getString(4));
		temp.setCodice(risultato.getString(5));
		
		return temp;
	}
	
	/**
	 * Metodo per la ricerca di una preonotazione a partire da un codice
	 * @param varCodice codice prenotazione da ricercare nel DB
	 * @return ritorna la prenotazione associata
	 */
	public Prenotazione getByCodice(String varCodice) throws SQLException{
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		Prenotazione temp = new Prenotazione();
		
		String query = "SELECT id, nome, cognome, orario, numeroPrenot FROM Prenotazione WHERE numeroPrenot = ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setString(1, varCodice);
		
		ps.executeQuery();
		
		ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	temp.setId(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setCognome(risultato.getString(3));
		temp.setOrario(risultato.getString(4));
		temp.setCodice(risultato.getString(5));
		
		return temp;
	}

	/**
	 * Metodo per l'estrazione dell'intero elenco di prenotazioni nel DB
	 * 
	 * @return un ArrayList di Prenotazioni contenente tutte le prenotazioni nel DB
	 */
	@Override
	public ArrayList<Prenotazione> getAll() throws SQLException {
		ArrayList<Prenotazione> elenco = new ArrayList<Prenotazione>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, cognome, orario, numeroPrenot FROM Prenotazione";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		
		ps.executeQuery();
		ResultSet risultato = ps.getResultSet();
		
		while(risultato.next()) {
			Prenotazione temp = new Prenotazione();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCognome(risultato.getString(3));
			temp.setOrario(risultato.getString(4));
			temp.setCodice(risultato.getString(5));
			
			elenco.add(temp);
		}
		
		return elenco;
	}

	/**
	 * Metodo per l'inserimento di una nuova prenotazione
	 * @param t oggetto prenotazione da inserire nel DB, deve essere compreso di codice autogenerato
	 * @return TRUE se l'inserimento è andato a buon fine || FALSE se l'inserimento non è riuscito
	 */
	@Override
	public void insert(Prenotazione t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Prenotazione(nome, cognome, orario, numeroPrenot) VALUE (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getCognome());
		ps.setDate(3, t.getOrario()); 
		ps.setString(4, t.getCodice());
		
		ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	/**
	 * Metodo per l'eliminazione di una prenotazione
	 * @param t oggetto prenotazione da eliminare dal DB
	 * @return TRUE se l'eliminazione è andata a buon fine || FALSE se l'eliminazione non è riuscita
	 */
	@Override
	public boolean delete(Prenotazione t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
   		String query = "DELETE FROM Prenotazione WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	/**
	 * Metodo per l'aggiornamento di una prenotazione.
	 * @param t oggetto prenotazione da aggiornare nel DB (con valori aggiornati)
	 * @return la prenotazione corretta corredata di tutti gli attributi || NULL se non riesce ad efffettuare la modifica
	 */
	@Override
	public Prenotazione update(Prenotazione t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE Prenotazione SET nome = ?, cognome = ?, orario = ? , numeroPrenot = ? WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getCognome());
       	ps.setDate(3, t.getOrario());
       	ps.setString(4, t.getCodice());
       	ps.setInt(5, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return getById(t.getId());
       	
   		return null;
	}

}
