package com.lezione31.PrenotazioniRistorante;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lezione31.PrenotazioniRistorante.models.Prenotazione;
import com.lezione31.PrenotazioniRistorante.services.PrenotazioneDAO;

@RestController
@CrossOrigin("http://localhost:8080")
@RequestMapping("/prenotazione")
public class PrenotazioneController {

	@GetMapping("/getOne/{codice}")
	public Prenotazione getPrenotazione(@PathVariable String codice) {
		PrenotazioneDAO preDAO = new PrenotazioneDAO();
		Prenotazione temp = null;
		try {
			temp = preDAO.getByCodice(codice);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return temp;
	}
	
	@GetMapping("/getAll")
	public ArrayList<Prenotazione> getPrenotazioni(){
		
		PrenotazioneDAO preDAO = new PrenotazioneDAO();
		ArrayList<Prenotazione> elenco = null;
		try {
			elenco = preDAO.getAll();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
	}
	
	//Non funziona
	@PostMapping("/insert")
	public Prenotazione insertPrenotazione(@RequestBody Prenotazione pren) {
		PrenotazioneDAO preDAO = new PrenotazioneDAO();
		Prenotazione temp = null;
		
		try {
			preDAO.insert(pren);
			temp = pren;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return temp;
	}
	
	@DeleteMapping("/delete/{codice}")
	public boolean deletePrenotazione(@PathVariable String codice) {
		PrenotazioneDAO preDAO = new PrenotazioneDAO();
		boolean esito = false;
		
		try {
			esito = preDAO.delete(preDAO.getByCodice(codice));
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return esito;
	}
	
	//Anche questa non funziona a causa del formato della data
	@PutMapping("/update")
	public Prenotazione updatePrenotazione(@PathVariable String codice, @RequestBody Prenotazione pren) {
		PrenotazioneDAO preDAO = new PrenotazioneDAO();
		Prenotazione temp = null;
		try {
			Prenotazione vecchia = preDAO.getByCodice(codice);
			
			//aggiorno le informazioni della vecchia
			vecchia.setNome(pren.getNome());
			vecchia.setCognome(pren.getCognome());
			vecchia.setOrario(pren.getOrarioS());
			vecchia.setCodice();
			
			temp = preDAO.update(vecchia);
			temp.setId(null); //nascondo l'ID all'utente
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return temp;
	}
}
