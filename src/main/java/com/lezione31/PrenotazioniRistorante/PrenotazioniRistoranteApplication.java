package com.lezione31.PrenotazioniRistorante;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrenotazioniRistoranteApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrenotazioniRistoranteApplication.class, args);
	}

}
